package nothing;

import org.apache.beam.sdk.options.ApplicationNameOptions;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.testing.TestPipeline;
import org.junit.jupiter.api.extension.*;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class TestPipelineExtension implements Extension, ParameterResolver, BeforeEachCallback, AfterEachCallback {

    private static final String TEST_PIPELINE = TestPipeline.class.getName();

    @Target(ElementType.PARAMETER)
    @Retention(RetentionPolicy.RUNTIME)
    @ExtendWith(TestPipelineExtension.class)
    public @interface BeamTestPipeline {

    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        PipelineOptions options = PipelineOptionsFactory.create();
        TestPipeline pipeline = TestPipeline.fromOptions(options);
        context.getStore(namespace()).put(TEST_PIPELINE, pipeline);

        // Work around because beam only supports junit 4
        Statement statement = pipeline.apply(null, Description.EMPTY);
        Method setDeducedEnforcementLevel = statement.getClass().getDeclaredMethod("setDeducedEnforcementLevel");
        setDeducedEnforcementLevel.setAccessible(true);
        setDeducedEnforcementLevel.invoke(statement);
        options.as(ApplicationNameOptions.class).setAppName(context.getDisplayName());
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return isTestPipeline(parameterContext);
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        if (isTestPipeline(parameterContext)) {
            return extensionContext.getStore(namespace()).get(TEST_PIPELINE, TestPipeline.class);
        }
        return null;
    }

    @Override
    public void afterEach(ExtensionContext context) throws Exception {

        // Work around because beam only supports junit 4
        TestPipeline pipeline = context.getStore(namespace()).remove(TEST_PIPELINE, TestPipeline.class);
        Field enforcementField = pipeline.getClass().getDeclaredField("enforcement");
        enforcementField.setAccessible(true);
        org.apache.beam.vendor.guava.v26_0_jre.com.google.common.base.Optional<?> enforcementOptional
                = (org.apache.beam.vendor.guava.v26_0_jre.com.google.common.base.Optional<?>) enforcementField.get(pipeline);
        Object enforcement = enforcementOptional.get();
        Method afterUserCodeFinished = enforcement.getClass().getDeclaredMethod("afterUserCodeFinished");
        afterUserCodeFinished.setAccessible(true);
        afterUserCodeFinished.invoke(enforcement);
    }

    private boolean isTestPipeline(ParameterContext parameterContext) {
        return parameterContext.getParameter().getType().equals(TestPipeline.class)
               && parameterContext.isAnnotated(BeamTestPipeline.class);
    }

    private static Namespace namespace() {
        return Namespace.create(TestPipelineExtension.class);
    }
}
